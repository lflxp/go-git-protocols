### go-git-protocols

用 Golang 实现 Git 不同传输协议的 Server 端

### 目录结构

```
.
├── LICENSE
├── README.md
├── git-server // git daemon
│   └── git-server.go
├── go.mod
├── go.sum
├── http-dumb  // http dump protocol
│   └── git-http-dumb.go
├── http-smart // http smart protocol
│   ├── git-http-smart.go
│   └── smart-utils.go
├── lib
│   └── common_utils.go
└── ssh-server // ssh server
    └── git-ssh-server.go
```

### 使用

进入到对应协议实现的目录，执行`go build`

以`ssh-server`为例

```
1. cd ssh-server    // 进入目录
2. go build         // 构建
3. ./ssh-server -h  // 根据提示加参数启动即可

➜  ssh-server git:(master) ✗ ./ssh-server -h
Usage of ./ssh-server:
  -hostkey string
        Specify a private key. (default "/etc/ssh/ssh_host_rsa_key")
  -port string
        Specify a port to start process. (default "8883")
  -repo string
        Specify a repositories dir. (default "/Users/zoker/Tmp/repositories/")
```

### 贡献代码

1. Fork 仓库
2. 创建本地分支 (`git checkout -b my-new-feature`)
3. 提交更改 (`git commit -am 'Add some feature'`)
4. 推送到分支 (`git push origin my-new-feature`)
5. 创建一个 Pull Request

### 贡献者

[@Zoker](https://zoker.io)